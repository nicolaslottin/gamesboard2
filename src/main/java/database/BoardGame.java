package database;

import java.awt.List;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import database.Database;
import java.sql.PreparedStatement;

/**
 * @author Nicolas
 */
public class BoardGame {
    protected int id;
    protected String titre;
    protected double prix;
    protected String joueur;
    protected int Stock;
    
    // Constructeur
    public BoardGame() {
    }
    
    // Constructeur
    public BoardGame(String ptitre, double pprix, String pjoueur, int pStock) {
        this.titre = ptitre;
        this.prix = pprix;
        this.joueur = pjoueur;
        this.Stock = pStock;
    }
    
    // Constructeur
    public BoardGame(int pid, String ptitre, double pprix, String pjoueur, int pstock) {
        this.id = pid;
        this.titre = ptitre;
        this.prix = pprix;
        this.joueur = pjoueur;
        this.Stock = pstock;
    }
    
    // Getter id
    public int getId() {
        return this.id;
    }
    
    // Getter title
    public String getTitle() {
        return this.titre;
    }
    
    // Getter price
    public double getPrice() {
        return this.prix;
    }
    
    // Getter players
    public String getPlayers() {
        return this.joueur;
    }
    
    // Getter in_stock
    public int getInStock() {
        return this.Stock;
    }
    
    // Setter title
    public void setTitle(String ptitre) {
        this.titre = ptitre;
    }
    
    // Setter price
    public void setPrice(double pprix) {
        this.prix = pprix;
    }
    
    // Setter players
    public void setPlayers(String pjoueur) {
        this.joueur = pjoueur;
    }
    
    // Setter in_stock
    public void setInStock(int pstock) {
        this.Stock = pstock;
    }
    
    
    // Permet d'obtenir tous les BoardGames stockés dans la BDD
    public static ArrayList<BoardGame> allGames() throws ClassNotFoundException, SQLException {   
        Connection connection = Database.getConnection();
        Statement statement = Database.createStatement(connection);
        String query = "SELECT * FROM BoardGame";
        ResultSet resultSet = Database.executeQuery(statement, query);
        
        ArrayList<BoardGame> liste = new ArrayList<>();
        BoardGame game;
        
        while(resultSet.next()){
            int pid = resultSet.getInt("id");
            String ptitre = resultSet.getString("titre");
            double pprix = resultSet.getDouble("prix");
            String pjoueur = resultSet.getString("joueur");
            int pstock = resultSet.getInt("stock");
            
            game = new BoardGame(pid, ptitre, pprix, pjoueur, pstock);
            
            liste.add(game);
        }
        resultSet.close();
        statement.close();
        connection.close();
             
        return liste;       
    }
    
    
    // Permet de récupérer les informations concernants un BoardGame
    public static BoardGame getBoardGame(int id) throws SQLException, ClassNotFoundException {
        BoardGame pgames;
        
        Connection connection = Database.getConnection();
        String query = "SELECT * FROM BoardGame WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        
        resultSet.next();
        int pid = resultSet.getInt("id");
        String ptitre = resultSet.getString("titre");
        double pprix = resultSet.getDouble("prix");
        String pjoueur = resultSet.getString("joueur");
        int pstock = resultSet.getInt("stock");
       
        pgames = new BoardGame(pid, ptitre, pprix, pjoueur, pstock);
        
        resultSet.close();
        preparedStatement.close();
        connection.close();
        
        return pgames;
    }
    
    
    // Permet d'ajouter un BoardGame
    public static void addBoardGame(BoardGame game) throws ClassNotFoundException, SQLException {
        Connection connection = Database.getConnection();
        String query = "INSERT INTO BoardGame (titre, joueur, Stock, prix) VALUES ((?), (?), (?), (?));";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setString(1, game.getTitle());
        preparedStatement.setString(2, game.getPlayers());
        preparedStatement.setInt(3, game.getInStock());
        preparedStatement.setDouble(4, game.getPrice());
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
    
    // Permet de supprimer un BoardGame
    public static void removeBoardGame(int pid) throws ClassNotFoundException, SQLException {
        Connection connection = Database.getConnection();
        String query = "DELETE FROM BoardGame WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setInt(1, pid);
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
    
    // Permet de modifier un BoardGame
    public static void updateBoardGame(int pid, BoardGame pnewGame) throws SQLException, ClassNotFoundException {
        Connection connection = Database.getConnection();
        String query = "UPDATE BoardGame SET title = (?), players = (?), in_stock = (?), price = (?)  WHERE id = (?);";
        
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setString(1, pnewGame.getTitle());
        preparedStatement.setString(2, pnewGame.getPlayers());
        preparedStatement.setInt(3, pnewGame.getInStock());
        preparedStatement.setDouble(4, pnewGame.getPrice());
        preparedStatement.setInt(5, pid);
        preparedStatement.executeUpdate();
        
        preparedStatement.close();
        connection.close();
    }
}
