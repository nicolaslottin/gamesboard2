
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Nicolas
 */
public class Database {
   protected static String dbPath;

    public static void setDbPath(String path) {
        Database.dbPath = String.format("jdbc:sqlite:%s", path);
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(Database.dbPath);
        return connection;
    }

    public static Statement createStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }

    public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
        return connection.prepareStatement(query);
    }

    public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
        return statement.executeQuery(query);
    }
    
    public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
        return preparedStatement.executeQuery();
    } 
}
